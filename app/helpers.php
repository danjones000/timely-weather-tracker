<?php
/**
 * This file contains custom helper functions.
 *
 * A lot of these functions are replacements for Laravel Facades, which aren't enabled by default in lumen.
 */

use Cmfcmf\OpenWeatherMap;
use Psr\Log\LoggerInterface;
use Illuminate\Contracts\Console\Kernel as ConsoleKernel;

/**
 * Returns a new instance of the OpenWeatherMap API.
 *
 * Uses OWM_API_KEY environment variable if no api-key is supplied.
 * @param  string $api_key
 * @return OpenWeatherMap
 */
function owm($api_key = null) {
    return new OpenWeatherMap($api_key ?: env('OWM_API_KEY'));
}

/**
 * Returns an instance of LoggerInterface. Useful for debugging.
 *
 * @return LoggerInterface
 */
function logger() {
    return app(LoggerInterface::class);
}

/**
 * Allows one to run artisan commands from within the app.
 *
 * If run with no arguments, simply returns the object. If run with arguments, runs call, and returns that function.
 *
 * @return ConsoleKernel|int
 */
function artisan($arg = null) {
    $artisan = app(ConsoleKernel::class);
    if (is_null($arg)) return $artisan;

    return call_user_func_array([$artisan, 'call'], func_get_args());
}
