<?php namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use InvalidArgumentException;

/**
 * Weather data for a particular point in time for a particular zip code.
 *
 * @property string $zip
 * @property string $city
 * @property string $country
 * @property float  $lat
 * @property float  $lon
 * @property string $conditions
 * @property float  $pressure (hPa)
 * @property float  $temp (℉)
 * @property array  $wind ['direction'=>int (degrees), 'speed'=>float (mph)]
 * @property float  $humidity (%)
 * @property Carbon $update
 */
class WeatherData extends Model
{
    use SoftDeletes;

    protected $table = 'weather_data';
    public $timestamps = true;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'update',
    ];

    protected $fillable = [
        'zip',
        'lat',
        'lon',
        'city',
        'country',
        'conditions',
        'pressure',
        'temp',
        'wind',
        'humidity',
        'update',
    ];

    // Relationships

    // Query Scopes

    // Accessors/Mutators

    public function getWindAttribute()
    {
        return [
            'direction' => $this->attributes['wind_direction'],
            'speed'     => $this->attributes['wind_speed'],
        ];
    }

    public function setWindAttribute($value)
    {
        if (!is_array($value) && !is_object($value)) {
            throw new InvalidArgumentException('wind attribute should be an array');
        }

        if (($direction = data_get($value, 'direction'))) {
            $this->attributes['wind_direction'] = $direction;
        }

        if (($speed = data_get($value, 'speed'))) {
            $this->attributes['wind_speed'] = $speed;
        }
    }
}
