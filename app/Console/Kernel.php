<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use InvalidArgumentException;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CheckWeather::class,
        Commands\WeatherList::class,
        Commands\WeatherListLocations::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     */
    protected function schedule(Schedule $schedule)
    {
        $weather_schedule = config('locations');
        foreach($weather_schedule as $zip => $args) {
            if (!is_string($args) && !is_array($args)) {
                throw new InvalidArgumentException('Schedule for weather check must be a string or array');
            }

            if (is_string($args)) {
                $frequency = $args;
                $args = [];
            } else {
                $frequency = array_shift($args);
            }

            $schedule->command('weather:check ' . $zip)->$frequency(...$args);
        }
    }
}
