<?php namespace App\Console\Commands;

use App\Model\WeatherData;
use Illuminate\Console\Command;

/**
 * Adds a command to list all locations for which we have historical data.
 */
class WeatherListLocations extends Command
{
    protected $signature = 'weather:list:locations';

    /**
     * Runs the command.
     */
    public function handle()
    {
        $locations = WeatherData::distinct()->select('zip')->get();
        $this->table(['zip'], $locations);
    }
}