<?php namespace App\Console\Commands;

use App\Model\WeatherData;
use Cmfcmf\OpenWeatherMap\Exception as OWMException;
use Illuminate\Console\Command;

class CheckWeather extends Command
{
    protected $signature = 'weather:check {zip=10001} {--s|skip-save : Skip saving to the database}';

    protected $description = 'Check the weather for a specific zipcode';

    /**
     * Execute the command.
     */
    public function handle()
    {
        $zip = $this->argument('zip');
        $this->info('Checking weather for ' . $zip);

        try {
            $data = owm()->getWeather('zip:' . $zip);
        } catch (OWMException $e) {
            $this->error('Error retriving data from OpenWeatherMap: ' . $e->getMessage());
            return 1;
        }

        $values = [
            'zip'        => $zip,
            'city'       => $data->city->name,
            'country'    => $data->city->country,
            'lat'        => $data->city->lat,
            'lon'        => $data->city->lon,
            'conditions' => $data->weather->description,
            'pressure'   => $data->pressure->getValue(),
            'temp'       => $data->temperature->getValue(),
            'humidity'   => $data->humidity->getValue(),
            'update'     => $data->lastUpdate,
            'wind'       => [
                'direction' => $data->wind->direction->getValue(),
                'speed'     => $data->wind->speed->getValue(),
            ]
        ];

        $wd = new WeatherData($values);
        $this->info("Current weather for {$wd->city} ({$wd->zip}): {$wd->conditions}");
        $this->info("Air Pressure: {$wd->pressure} hPa");
        $this->info("Temperature: {$wd->temp} ℉");
        $this->info("Wind: {$wd->wind['speed']} mph at {$wd->wind['direction']}°");
        $this->info("Humidity: {$wd->humidity}%");
        $this->info('');
        $this->info("Last updated at {$wd->update->format('r')}.");
        
        if (!$this->option('skip-save')) {
            $this->info('');
            $this->info('Saving to database.');
            $wd->save();
            $this->info('Done!');
        }
    }
}
