<?php namespace App\Console\Commands;

use App\Model\WeatherData;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

/**
 * Adds a command to list all historical data for a given location.
 */
class WeatherList extends Command
{
    protected $signature = 'weather:list {zip=10001 : The zipcode of the location} ' .
                         '{--m|number= : The number of historical entries to retrieve} ' .
                         '{--f|from= : Earliest date to retrieve} ' .
                         '{--t|to= : Latest date to retrieve}';

    /**
     * Generate a query based upon the supplied command line arguments.
     *
     * @param  Builder $query
     * @return Builder
     */
    protected function build_query(Builder $query) : Builder
    {
        $query->where(['zip' => $this->argument('zip')]);
        if (($from = $this->option('from'))) {
            $query->where('update', '>=', new Carbon($from));
        }
        if (($to = $this->option('to'))) {
            $query->where('update', '<=', new Carbon($to));
        }
        return $query;
    }

    /**
     * Runs the command.
     */
    public function handle()
    {
        $city_columns = ['city', 'country', 'zip'];
        $city = $this->build_query(WeatherData::select($city_columns))->first();

        $data_columns = ['conditions', 'pressure', 'temp', 'humidity', 'update'];
        $display_columns = [
            'conditions',
            'pressure (hPa)',
            'temperature (℉)',
            'humidity (%)',
            'time recorded',
        ];
        $data = $this->build_query(WeatherData::select($data_columns));
        if (($number = $this->option('number'))) {
            $data = $data->take($number);
        }

        if (empty($city)) {
            $this->error('No data found for zip code ' . $this->argument('zip'));
            return 1;
        }

        $this->table($city_columns, collect([$city]));
        $this->table($display_columns, $data->get());
    }
}