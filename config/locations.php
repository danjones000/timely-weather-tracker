<?php
/**
 * This file contains locations that should be checked for current weather conditions.
 * Each location must be specified with a zip code as the key, and the frequency as the value.
 * Frequency may be any of the valid scheduling frequency options for task scheduling.
 *
 * For frequency that require parameters, the value should be an array, in which the first item is the frequency,
 * and additional items are the parameters.
 *
 * Here are some valid examples:
 *   [
 *     '12345' => 'hourly',                  // search every hour
 *     '23456' => 'everyMinute',             // search every minute
 *     '34567' => ['hourlyAt', 15],          // search every hour at :15 past
 *     '45678' => ['monthlyOn', 4, '15:00'], // search every month on the fourth at 3pm
 *     '56789' => ['cron', '* 2 * * * *'],   // daily at 2am
 *   ]
 *
 * @see https://laravel.com/docs/5.4/scheduling
 */

return [
    '77070' => 'hourly',
    '10001' => 'everyThirtyMinutes',
];
