<?php namespace Tests\Model;

use App\Model\WeatherData;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Database\QueryException;
use PHPUnit_Framework_ExpectationFailedException;

class WeatherDataTest extends TestCase
{
    /**
     * Provides a set of valid properties for WeatherData constructor.
     *
     * @return array
     */
    public function constructorValidArgumentsProvider()
    {
        return ['all' => [[
            'zip' => 12345,
            'lat' => 29.98,
            'lon' => -95.58,
            'city' => 'Houston',
            'country' => 'US',
            'conditions' => 'light rain',
            'pressure' => 1011.0,
            'temp' => 90,
            'wind' => [
                'direction' => 160,
                'speed' => 4.7
            ],
            'humidity' => 90,
            'update' => Carbon::now(),
        ]]];
    }

    /**
     * Tests the constructor with valid values.
     *
     * @throws PHPUnit_Framework_ExpectationFailedException
     * @throws QueryException if save fails.
     *
     * @dataProvider constructorValidArgumentsProvider
     */
    public function testConstructorValid($values)
    {
        $wd = new WeatherData($values);
        $this->assertInstanceOf(WeatherData::class, $wd);

        $wd->save();
        $wd->forceDelete();
    }

    /**
     * Tests creating and saving new WeatherData instances with a missing value.
     *
     * @throws PHPUnit_Framework_ExpectationFailedException
     *
     * @dataProvider constructorValidArgumentsProvider
     */
    public function testConstructorInvalid($values)
    {
        // We loop throw the array, setting each one as null, and attempting to save it.
        foreach($values as $key => $value)
        {
            $missing = $values;
            $missing[$key] = null;

            $wd = new WeatherData($missing);
            $this->setExpectedException(QueryException::class);
            $wd->save();
        }
    }
}
