<?php namespace Tests;

use Cmfcmf\OpenWeatherMap;

class OWMHelperTest extends TestCase
{
    /**
     * Tests that the omw helper function functions.
     *
     * @throws \PHPUnit_Framework_ExpectationFailedException
     */
    public function testOWMHelper()
    {
        $constructed = new OpenWeatherMap(env('OWM_API_KEY'));
        $helped = owm();
        $helped_with_key = owm(env('OWM_API_KEY'));

        $this->assertEquals($constructed, $helped);
        $this->assertEquals($constructed, $helped_with_key);
        $this->assertEquals($helped, $helped_with_key);
    }
}
