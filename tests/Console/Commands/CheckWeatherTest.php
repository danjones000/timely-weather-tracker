<?php namespace Tests\Console\Commands;

use App\Model\WeatherData;
use Carbon\Carbon;
use Tests\TestCase;

class CheckWeatherTest extends TestCase
{
    /**
     * Ensure the command runs while saving the data.
     */
    public function testRunWithSave()
    {
        $initial = WeatherData::count();
        artisan('weather:check');
        $after = WeatherData::count();

        $this->assertEquals($initial+1, $after);

        // Cleanup by deleting the model
        WeatherData::orderBy('created_at', 'desc')->first()->forceDelete();
    }

    /**
     * Ensures the command runs without saving the data.
     */
    public function testRunWithoutSave()
    {
        $initial = WeatherData::count();
        artisan('weather:check', ['-s' => true]);
        $after = WeatherData::count();

        $this->assertEquals($initial, $after);
    }

    /**
     * Ensure the command succesfully runs with a zip code.
     */
    public function testWithZip()
    {
        artisan('weather:check', ['zip' => '77095']);
        $wd = WeatherData::where('zip',  '=', '77095')->where('created_at', '>', new Carbon('now - 1 minute'))->first();

        $this->assertInstanceOf(WeatherData::class, $wd);

        // Cleanup
        $wd->forceDelete();
    }
}
