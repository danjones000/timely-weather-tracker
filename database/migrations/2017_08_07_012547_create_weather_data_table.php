<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Create weather_data table
 */
class CreateWeatherDataTable extends Migration
{
    /**
     * Create table
     */
    public function up()
    {
        Schema::create('weather_data', function (Blueprint $table) {
                $table->increments('id');
                $table->string('zip');
                $table->string('city');
                $table->string('country');
                $table->decimal('lat', 9, 6);
                $table->decimal('lon', 9, 6);
                $table->string('conditions');
                $table->decimal('pressure', 7, 3);
                $table->decimal('temp', 7, 4);
                $table->integer('wind_direction');
                $table->decimal('wind_speed', 5, 3);
                $table->decimal('humidity', 5, 2);
                $table->timestamp('update');

                $table->timestamps();
                $table->softDeletes();
            });

    }

    /**
     * Drop table
     */
    public function down()
    {
        Schema::drop('weather_data');
    }
}
