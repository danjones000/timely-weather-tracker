# Timely Weather Tracker

Track the weather for specific locations (specified in a configuration file).

## Description

- Create an application that will track current weather measurements for a given set of zip codes
- Store the following data at a minimum:
    + Zip code,
    + General weather conditions (e.g. sunny, rainy, etc),
    + Atmospheric pressure,
    + Temperature (in Fahrenheit),
    + Winds (direction and speed),
    + Humidity,
    + Timestamp (in UTC)
- There is no output requirement for this application, it is data retrieval and storage only
- The application should be able to recover from any errors encountered
- The application should be developed using a TDD approach. 100% code coverage is not required
- The set of zip codes and their respective retrieval frequency should be contained in configuration file
- Use the OpenWeatherMap API for data retrieval (https://openweathermap.org)

## Implementation

I used the [Lumen framework](https://lumen.laravel.com/) to facilitate development. One benefit of this is that it could be expanded into an API with the addition of a few routes.

To connect to [OpenWeatherMap](https://openweathermap.org/), I used the [OpenWeatherMap PHP Api](https://github.com/cmfcmf/OpenWeatherMap-PHP-Api/). When I began work on this, the API didn't support querying the API based on zip codes, so I created [my own fork](https://github.com/goodevilgenius/OpenWeatherMap-PHP-Api/) which implemented this feature. I also opened [a pull request](https://github.com/cmfcmf/OpenWeatherMap-PHP-Api/pull/106) with the main project to implement my changes, which was then merged in.

The configuration is stored in a standard Laravel/Lumen [configuration file](config/locations.php), which can be retrieved using the helper method `config('locations')`. The file was documented to explain how it should be written.

An [Eloquent Model](app/Model/WeatherData.php) for the retrieved data was created, as well as a [migration](database/migrations/2017_08_07_012547_create_weather_data_table.php) to create the appropriate table in the database.

An [artisan command (weather:check)](app/Console/Commands/CheckWeather.php) was created that allows for checking weather data, and adding it to the database, and a schedule, using the configuration file, [was added](app/Console/Kernel.php).

Testing was done using PHPUnit, and TDD methodology was utilized.

## Additions

Two artisan commands, [weather:list](app/Console/Commands/WeatherList.php) and [weather:list:locations](app/Console/Commands/WeatherListLocations.php) were added to allow for listing the historical weather data, as well as all known locations.

## Installing

1. Clone the repository, and run `composer install` to install any dependencies.
2. Set up a SQL database of your choosing (sqlite is suitable for this), and add the necessary configuration to the `.env` file.
3. Modify the [schedule configuration](config/locations.php) to include any locations you'd like archived.
4. Add an entry to your crontab so that Lumen's scheduler will run. It should look like this:

        * * * * * php /path/to/installation/artisan schedule:run

## Future enhancements

* API Routes to retrieve the data could be added.
* Storing the schedule in a database, and adding additional API routes to add new schedules may be useful.
